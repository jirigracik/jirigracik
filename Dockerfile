FROM node:16-alpine AS build

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY package*.json /usr/src/app/

RUN npm install

COPY . /usr/src/app

RUN npm run build

FROM nginx:1.19-alpine
COPY --from=build /usr/src/app/dist/jirigracik /app
